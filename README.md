Setup the zsh configuration
===========================

Run the script install.sh to create ~/.zsh directory, copy zshrc to ~/.zshrc and other conf in ~/.zsh/.

This script will **overwrite** the configuration files, if you want to customize, create other config files, like aliases.local.
