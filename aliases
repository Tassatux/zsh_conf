#!/usr/bin/env zsh
#        _ _
#   __ _| (_) __ _ ___  ___  ___
#  / _` | | |/ _` / __|/ _ \/ __|
# | (_| | | | (_| \__ \  __/\__ \
#  \__,_|_|_|\__,_|___/\___||___/
#
#
# To temporarily bypass an alias, we preceed the command with a \
#

##########################
##        colors        ##
##########################

if [ -x /bin/dircolors ] || [ -x /usr/bin/dircolors ]; then
    eval "`dircolors -b`"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias grepc='grep --color=always'

    if [ -x /usr/bin/colordiff ]; then
        alias diff='colordiff'
    fi

    if [ -f /etc/gentoo-release ]; then
        alias eix="eix -F"
    fi
fi

##########################
##          ls          ##
##########################

alias ll='ls -lh'
alias la='ls -Ah'
alias l='ls -CF'
alias lla='ls -lAh'
alias lsc='ls --color=always'
alias llc='ll --color=always'
alias llac='lla --color=always'

##########################
##         misc         ##
##########################

alias s.='source ~/.zshrc'
alias zshrc='vi ~/.zshrc'
alias nalias='vi ~/.zsh/aliases.local'
alias nfunction='vi ~/.zsh/functions.local'
alias vimrc='vi ~/.vimrc'
alias psall='ps -o user,pid,pgid,sid,cmd'
alias gdbbt='gdb -q -n -ex bt -batch'
alias pyserv='python2 -m SimpleHTTPServer'
alias genpasswd='apg -a 0 -n 20 -m 12 -x 20 -M SNCL -E "{[]}\|^"'
alias t='todo.sh'
alias clean_svn='find . -depth -name .svn -type d -exec rm -fr {} \;'
alias hc='sed -r "/^(\s*#.*)?$/d"'
alias ipsort='sort -t . -k 1,1n -k 2,2n -k 3,3n -k 4,4n'

##########################
##         ssh          ##
##########################

alias sshu='ssh -o UserKnownHostsFile=/dev/null'
alias sr='ssh -l root'
alias sru='ssh -l root -o UserKnownHostsFile=/dev/null'

##########################
##         jump         ##
##########################

alias jlog='cd /var/log/'

##########################
##        distrib       ##
##########################

# Archlinux specific aliases
if [ -f /etc/arch-release ]; then
    if [ -x /usr/bin/yay ]; then
        # Synchronize with repositories before upgrading packages that are out
        # of date on the local system
        alias uu='yay -Syu'
        # Install specific package(s) from the repositories
        alias pacinstall='yay -S'
        # Install specific package not from the repositories as a file
        alias pacfile-install='yay -U'
        # Remove the specified package(s), retaining its configuration(s) and
        # required dependencies
        alias pacremove='yay -R'
        # Remove the specified package(s), its configuration(s) and unneeded
        # dependencies
        alias pacpurge='yay -Rns'
        # Display information about a given package in the repositories
        alias pacshow='yay -Si'
        # Display information about a given package in the local database
        alias paclocal-show='yay -Qi'
        # Search for package(s) in the local database
        alias paclocal-search='yay -Qs'
        # List package content
        alias paclist='yay -Ql'
        # Search for package(s) in the repositories
        alias ysearch='yay -Ss'
    else
        alias uu='pacman -Syu'
        alias pacinstall='pacman -S'
        alias pacfile-install='pacman -U'
        alias pacremove='pacman -R'
        alias pacpurge='pacman -Rns'
        alias pacshow='pacman -Si'
        alias paclocal-show='pacman -Qi'
        alias paclocal-search='pacman -Qs'
        alias paclist='pacman -Ql'
        alias pacsearch='pacman -Ss'
        alias ysearch='pacsearch'
    fi
# Debian specific aliases
elif [ -f /etc/debian_version ]; then
    alias prename='file-rename'
    alias uu='aptitude update; aptitude safe-upgrade && aptitude full-upgrade'
    alias pacinstall='aptitude install'
    alias pacfile-install='dpkg -i'
    alias pacremove='aptitude remove'
    alias pacpurge='aptitude purge'
    alias paclist='dpkg -L'
    alias rpaclist='apt-file list'
    alias pacshow='aptitude show'
    alias pacsearch='aptitude search'
    alias ysearch='pacsearch'
    alias pkgfile='apt-file'
    # alias apolicy='apt-cache policy'
fi

##########################
##       overload       ##
##########################

alias less='less -m'
alias les='less'
alias ms='ls'
alias rm='rm -I --preserve-root'
alias nano='nano -w'
alias vi='vim'
alias du='du -h'
alias df='df -h'
alias rsync='rsync -h --progress'
alias prename='perl-rename'
alias halt='poweroff'

##########################
##       suffixes       ##
##########################

alias -s png=geeqie
alias -s jpeg=geeqie
alias -s jpg=geeqie
alias -s bmp=geeqie
alias -s gif=geeqie
alias -s doc=libreoffice -writer
alias -s odt=libreoffice -writer
alias -s pdf=evince
alias -s mp3=mpv
alias -s ogg=mpv
alias -s wav=mpv
alias -s avi=mpv
alias -s mpg=mpv
alias -s wmv=mpv
alias -s mpeg=mpv
alias -s txt=less
alias -s c=vim
alias -s java=vim
alias -s cpp=vim
alias -s h=vim
alias -s s=vim
alias -s php=vim
alias -s html=firefox
alias -s gz=tar xvfz
alias -s bz2=tar xvfj

##########################
##        global        ##
##########################

alias -g L='| less'
alias -g H='| head'
alias -g G='| grep'
alias -g T='| tail'
alias -g C='| wc -l'
alias -g M='| most'
alias -g S='| sort -h'
alias -g B='&|'
alias -g HL='--help'
alias -g LL='2>&1 | less'
alias -g CA='2>&1 | cat -A'
alias -g NE='2> /dev/null'
alias -g NUL='> /dev/null 2>&1'
alias -g V='--version'
alias -g BG='& disown && exit'
