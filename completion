#!/usr/bin/env zsh
#
#                            _      _   _
#   ___ ___  _ __ ___  _ __ | | ___| |_(_) ___  _ __
#  / __/ _ \| '_ ` _ \| '_ \| |/ _ \ __| |/ _ \| '_ \
# | (_| (_) | | | | | | |_) | |  __/ |_| | (_) | | | |
#  \___\___/|_| |_| |_| .__/|_|\___|\__|_|\___/|_| |_|
#                     |_|
#

# Automatically list choices on an ambiguous completion.
setopt auto_list

# Display completion list even if there is an unambiguous prefix to insert on
# the command line.
unsetopt list_ambiguous

# Automatically use menu completion after the second consecutive request for
# completion, for example by pressing the tab key repeatedly.
setopt auto_menu

# When the last character resulting from a completion is a slash and the next
# character typed is a word delimiter, a slash, or a character that ends a
# command (such as a semicolon or an ampersand), remove the slash.
setopt auto_remove_slash

# Formatting
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'

# Display menu only if at least 2 matches
zstyle ':completion:*' menu select=2
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s

# Use cache for completion
zstyle ':completion:*' use-cache true
zstyle ':completion:*' cache-path "$HOME/.zcompcache"

# Add colors for completion
# require module zsh/complist
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

# Enable colors and custom completion for command 'kill'
zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#)*=36=31"
zstyle ':completion:*:processes' command 'ps -au$USER'
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*'   force-list always

# Sort completion matches per group
zstyle ':completion:*' group-name ''

# Separate man-page completion by section
zstyle ':completion:*:manuals' separate-sections true

# Enable case-insensitive completion
zstyle ':completion:*:complete:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'

# Use ip for host completion
zstyle ':completion:*' use-ip true

# Define custom pattern (and ignore pattern) for some softwares
zstyle ':completion:*:*:zless:*' file-patterns '*(-/):directories *.gz:all-files'
zstyle ':completion:*:*:evince:*' file-patterns '*(-/):directories (#i)*.(pdf|ps)'
zstyle ':completion:*:*:epdfview:*' file-patterns '*(-/):directories (#i)*.(pdf)'
zstyle ':completion:*:*:vim:*' ignored-patterns '*.(o|pyc)'
zstyle ':completion:*:*:less*:*' ignored-patterns '*.(o|class)'
zstyle ':completion:*:cd:*' ignored-patterns '(*/)#lost+found'

# Ignore system users when complete a user
zstyle ':completion:*:*:*:users' ignored-patterns \
    adm apache bin daemon games gdm halt ident junkbust lp mail mailnull \
    named news nfsnobody nobody nscd ntp operator pcap postgres radvd \
    rpc rpcuser rpm shutdown squid sshd sync uucp vcsa xfs backup  bind  \
    dictd  gnats  identd  irc  man  messagebus  postfix  proxy  sys  www-data

# Ignore zsh completion function (which start with an '_')
zstyle ':completion:*:functions' ignored-patterns '_*'

# Ignore completions matches already on the line for some software
zstyle ':completion:*:(rm|kill|diff|yaourt|pacman|emerge|aptitude):*' ignore-line yes

# Force path update during completion (to find new installed commands)
_force_rehash()
{
    (( CURRENT == 1 )) && rehash
    return 1
}

# Completion pattern: try to complete by approximation when misstyping and
# force rehash
zstyle ':completion:*:match:*' original only
zstyle ':completion:::::' completer _force_rehash _complete _match _approximate
#
# Error in completion depends of the size of that we need to approximate
zstyle -e ':completion:*:approximate:*' max-errors 'reply=( $(( ($#PREFIX + $#SUFFIX) / 3 )) )'

# Don't add the current directory in completion of parents, e.g: if pwd = foo,
# cd ../ never complete with 'foo'
zstyle ':completion:*:(cd|ls|mv|cp|rsync|rm):*' ignore-parents parent pwd
