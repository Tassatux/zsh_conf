#!/bin/sh

# Ensure we are in the script directory to copy zsh config
cd $(dirname $0)

mkdir -p ~/.zsh
cp zshrc ~/.zshrc
cp ./aliases ./bindkeys ./completion ./export ./functions ./history ./prompt ~/.zsh/
