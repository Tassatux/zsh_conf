#!/usr/bin/env zsh
#     _________  _   _ ____   ____
#    |__  / ___|| | | |  _ \ / ___|
#      / /\___ \| |_| | |_) | |
#  _  / /_ ___) |  _  |  _ <| |___
# (_)/____|____/|_| |_|_| \_\\____|
#

ZSH_CONF_DIR="${HOME}/.zsh"

########################################
##          Color variables           ##
########################################

autoload -U colors && colors
light_black="%{$fg[black]%}"
light_green="%{$fg[green]%}"
light_red="%{$fg[red]%}"
light_yellow="%{$fg[yellow]%}"
light_blue="%{$fg[blue]%}"
light_cyan="%{$fg[cyan]%}"
light_purple="%{$fg[magenta]%}"
black="%{$fg_bold[black]%}"
green="%{$fg_bold[green]%}"
red="%{$fg_bold[red]%}"
yellow="%{$fg_bold[yellow]%}"
blue="%{$fg_bold[blue]%}"
cyan="%{$fg_bold[cyan]%}"
purple="%{$fg_bold[magenta]%}"
reset="%{${reset_color}%}"

########################################
##            Terminal title          ##
########################################

# src : http://bbs.archlinux.org/viewtopic.php?pid=258216#p258216

THSTATUS=`tput tsl`
FHSTATUS=`tput fsl`
# set_xterm_title() sets the title of an xterm. It's now terminfo-enabled.
# While this is the "right way", it also means you need a sane terminfo entry
# for your terminal.
set_xterm_title() {
    if tput hs ; then
        print -Pn "$THSTATUS$@$LOCAL_CUSTOM_HARDSTATUS$FHSTATUS"
    fi
}

# Add a custom tag in terminal title
tag() {
    if [ $# -eq 0 ] ; then
        LOCAL_CUSTOM_HARDSTATUS=""
    else
        LOCAL_CUSTOM_HARDSTATUS=" - $@"
    fi
}

# ZSH runs precmd() before each prompt. Because of the above preexec hack, I
# use it to display the pwd in the titlebar. Most people use chpwd for this,
# which is a bit more efficient, but that obviously wouldn't work in this case.
precmd() {
    vcs_info # retrieve information to update vcs info
    set_xterm_title "%n@%m: %50<...<%~%<<"
}

########################################
##         Changing Directories       ##
########################################

# If a command is issued that can't be executed as a normal command, and the
# command is the name of a directory, perform the cd command to that directory.
setopt autocd

# Make cd push the old directory onto the directory stack.
setopt autopushd
# Do not print the directory stack after pushd or popd.
setopt pushd_silent

# Resolve symbolic links to their true values when changing directory. This
# also has the effect of CHASE_DOTS, i.e. a `..' path segment will be treated
# as referring to the physical parent, even  if  the  preceding  path segment
# is a symbolic link.
setopt chase_links

########################################
##              Globbing            ##
########################################

# Treat the `#', `~' and `^' characters as part of patterns for filename
# generation, etc.  (An initial unquoted `~' always produces named directory
# expansion.)
setopt extendedglob

# If a pattern for filename generation has no matches, delete the pattern from
# the argument list instead of reporting an error.
# /!\ Disabled to avoid awful side effects, for example if you type 'rm foo*' and
# foo* doesn't match anything, 'foo' will be removed and '*' will match
# everything... /!\
unsetopt nullglob

########################################
##             Inputs/Output        ##
########################################

# Try to correct the spelling of commands.
setopt correct

# Query the user before executing `rm *' or `rm path/*'
unsetopt rm_star_silent

# Print the exit value of programs with non-zero exit status.
setopt print_exit_value

# Print a warning message if a mail file has been accessed since the shell last
# checked.
setopt mail_warning

# Allow comments even in interactive shells.
setopt interactive_comments

########################################
##             Job Control            ##
########################################

# Run all background jobs at a lower priority. (Disabled)
unsetopt bg_nice

# Report  the  status of background and suspended jobs before exiting a shell
# with job control; a second attempt to exit the shell will succeed.
setopt check_jobs

########################################
##         Scripts and Functions      ##
########################################

# When executing a shell function or sourcing a script, set $0 temporarily to
# the name of the function/script.
setopt function_argzero

# Perform implicit tees or cats when multiple redirections are attempted.
setopt multios

########################################
##               Modules              ##
########################################

# Standard scientific functions for use in mathematical evaluations.
zmodload zsh/mathfunc

# The Zsh Line Editor, including the bindkey and vared builtins.
zmodload zsh/zle

# Completion listing extensions.
zmodload zsh/complist

########################################
##             Config files           ##
########################################

# Load global conf
for conf in ${ZSH_CONF_DIR}/^*.local; do
    . $conf
done
# Then load local
for conf in ${ZSH_CONF_DIR}/*.local; do
    . $conf
done

########################################
##            Miscellaneous           ##
########################################

# Dump core files
ulimit -c unlimited

if [[ -d ~/.zsh/completions ]]; then
    fpath=(~/.zsh/completions $fpath)
fi

# Run compinit after loading every configuration file (if we need to include
# custom completion)
autoload -U compinit && compinit
